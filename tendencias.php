<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>M.Y.S</title>
    
    
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
     crossorigin="anonymous">    
     <!-- fontawsome-5 -->
     <script src="https://kit.fontawesome.com/3bbdd6b5d0.js" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="estilos.css">
</head>
<body style="background: url(https://freshome.com/wp-content/uploads/2019/05/bad-living-room-cleaning-habits.jpg);" >
    
    
    <div id="contenedor_subasta" style=" height:150px ;background-color:#2c3e50;border-radius:10px "  class="container my-2">
       <nav id="nav" style="  height:150px"  class="" >
           <a id="mys"   class="navbar navbar-brand" style="color:white ;height:80px;width:170px;font-size:55px;" href="index.php">M.Y.S</a>
          <p style="height:50px;" id="parra"  >En tendencias, desde M.Y.S te damos algunas <strong>ideas</strong> para tu hogar,
             muchas veces ver otros proyectos nos ayuda a terminar el nuestro .Descubre las ultimas tendencias en <strong>menaje del hogar aqui</strong>
          </p>
          <ul  style=" width:450px  ;margin-right:20px " class="nav nav-tabs">
             <li class="nav-item">
                <a class="nav-link text-white "  href="#baños">Baños</a>
             </li>
             <li class="nav-item">
                <a class="nav-link text-white " href="#salon">Salon</a>
             </li>
             <li class="nav-item">
                <a class="nav-link text-white"  href="#cocinas">Cocinas</a>
             </li>
             <li class="nav-item">
                <a class="nav-link text-white"  href="index.php">Inicio</a>
             </li>
          </ul>
         
       </nav>   
   </div>

   <div id="baños"   class="container my-auto mx-auto">
        <h3 id="tendencias_baños" class="my-2" >Baños</h3>
        <div class=" bg-dark text-white " style=" height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
           
            <img style="width:230px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:35px " src="https://www.banium.com/wp-content/uploads/2018/08/grifo-de-lavabo-geo-augeo010-amb5_resultado.jpg" alt="griferia en cascada">
              <p style="width:450px ;float:right; margin-right:30px ;margin-top:30px" id="griferia"  >
                  Seguro!!... los <strong>grifos de cascada </strong> es lo mas usado ,un diseño que nos deja un paso
                   mas cerca del estado mas natural del agua ,particularmente puedo decir que la primera vez que lo vi me quede fascinado , ver  la superficie de la griferia a traves de las 
                  transparencias del agua me detuvo 3 minutos de reloj abriendo y cerrando el grifo. He de confesar que la segunda reaccion fue preguntar el precio , pues bien 
                  esa fue otra grata sorpresa, desde 35€ puede ser tuya incluso con luces .Cree cuando te digo que la sensacion es muy buena cuando una visita te pregunta por el baño
                  <a style="margin-left: 70px;" href="https://www.youtube.com/watch?v=WUCxuFgIAGo">Disfruta el video</a> 
              </p>
        </div>
        <div class=" bg-dark text-white " style=" height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
        
          <img style="width:230px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:35px "  src="https://cdn.manomano.com/lavabo-cuadrado-bano-ceramica-pila-lavamanos-sobre-encimera-suspendido-350x205mm-P-8912160-18239713_1.jpg" alt="">
          <p style="width:450px ;float:right; margin-right:30px ;margin-top:30px" >Ahi veces en las que el <strong>espacio</strong> hay que aprovecharlos al maximo
               , sobre todo , si vives en el centro de una gran urbe o alquilas tu departamento con una agencia de alquiler turistico , este estilo algo minimalista
               es necesario y esta en armonia con espacios pequeños , lo que quieres es un baño de 250X150 cm es poder dar media vuelta sin complicarte mucho 
               ,usar encimeras de madera con una pila blanca como en la foto, es en mi opinion una combinacion excelente . Sè lo que estas pensando... "¿madera en un baño?"
               la madera de teca no tiene problemas.
          </p>
        </div>

        <div class=" bg-dark text-white " style=" height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
            <img  style="width:230px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:35px " src="https://www.bazarvintage.es/wp-content/uploads/0c67811be0d5-400x400.jpg" alt="">
            <p style="width:450px ;float:right; margin-right:30px ;margin-top:30px" >
               Bañeras vintage o bañeras exenta ,si eres un cinefilo ,este puede ser uno de tus grandes deseos ,ya que ,pareciera que no hay pelicula en la que ya no salga ,si bien es cierto necesitas un baño bastante grande, unas 
               observaciones que podemos hacer es que estas bañeras estan hechas de una sola pieza ,para evitar cualquier tipo de filtraciones ,suelen estar 
               compuestas por capas de 4 a 8 milimetros de un acrilico muy resistente llamado <strong> Resicryl</strong> y un refuerzo de fibra de vidrio ,el sifon y el desague
               suelen estar en la parte mas escondida y son macizos, asi que el riesgo de filtracion es nulo .
               <a href="https://www.youtube.com/watch?v=1MRkIscAK-A">Estos son algunos ejemplos</a>
            </p>
        </div>
           
        <div class=" bg-dark text-white " style=" height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
        <img  style="width:230px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:35px "  src="https://www.azulejospena.es/wp-content/uploads/2018/07/classic-mueble-420x420.jpg" alt="">
        <p style="width:450px ;float:right; margin-right:30px ;margin-top:30px" >
             Parece obvio a estas alturas "pedir" un baño con muebles <strong>suspendidos</strong> pero vamos a enfatizar un poco sobre ello .Si ,es realmente comodo
             poder acceder a todos los complementos de baño sin tener que agacharse o ponerse de cuclillas , es por eso que los muebles de baño por lo general no tinen
             patas ni rodapie ,una observacion si te tomas en serio nuestro Lema de "Make by yourself" , ten cuidado donde taladras para colgar estos muebles ya que podrian pasar tuberias
             y ya sabes los riesgos que eso conlleva , fuera de eso es la mejor opcion para el baño sin lugar a dudas.
    
          </p>
        </div>
   </div>
   <div  class="my-auto mx-auto " >
      
      <div style="margin-left:90px;margin-bottom:30px" >
         <h3 id="salon" style="border:1px solid bla  " class="my-2 text-white"  >Salon</h3>
         <a  href="#baños" class="badge badge-success">Baños</a>
         <a href="#cocinas" class="badge badge-dark">Cocinas</a>
         <a href="#mys" class="badge badge-warning">Inicio</a>
      </div>

      <div class=" bg-dark text-white " style=" margin-left:95px; height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
         <img  style="width:290px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:35px "  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhUSEBIVFRUVFRUVFRUWGBcWFxcXFRUXFxUVFxUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy8dHSUtKy0tLS0tLS0tLS0rLS0tKy0tKystKy0tLS0tLS0tLS0tLS0tLS0tLS0tLSstNy0tLf/AABEIAMIBAwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIDBQYEBwj/xABFEAABBAAEAwQGBggEBQUAAAABAAIDEQQSITEFQVEGYXGBEyIykaGxI0JScsHRFDNigpKy4fAHc6LCFRY0ZPEXJENTY//EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/8QAIhEBAQACAgICAwEBAAAAAAAAAAECERIhAzETQSIyUWFx/9oADAMBAAIRAxEAPwD11KhCBUISoBCEqAQhKgEISoBCEIBKhCAQhKgE1+ycmv2QRJEqECJUIQCEIQCVIlQCVAQgVCEIFQhCASpEII0qEqBEqEqAQlQgEJUIESoSoESoSoEQlQgEJUIETZNk9NfsgiSJUiAQhCAQopMSxppzgD0vX3JBi2ciT4NcfkE2aToUP6QPsv8A4T+KdDKHbWK6qbEqVIlVAlSJUAhCEAhIlQNQlQgEqAlQCEJUAhCVAiVCEAlQhAIQlQCEJUCJsmyemybIIUiVNKASFKmlByOnDPSOOzQCa81wntJDya8+TfzU2PaCycE1bavpdrxrHYCbh74nxSFzXDUCwDVWHN5iiO8a7Kwr2PBcdbJI1gY4Zr1JHIE7eSsMH9b7x+ZWD7NcXjdi8PHRzSMLx0FxOcNTvsdui3eD+t94/wAxUv0R1JUiLQKhIlQFoQhAqEiVAiVCVAIQlQCVCEAlQhAIQhAIQhAJHvAFk0Eq4eLNJbXLnyUyupuDtjladj/ZsD5FPBWahxBZz02vpuBf8R17/NQ43jEkUmlhgGg03ySZW+8D/SuOPn37GrTZNk3DTB7Q4bEf+fjaWXZdxCkJSEpCUBaQlISmuKDkkP62+g/FeT/4m4hmeHI85msJc2vULS4NLy7kRl89V6nPmIly6urQd+tLyvtx2L4liHsfDAHsa0NLMzGuLg55zG3DSiOYOvmk9CD/AA3mDsfh6JfTnhpsU1ogmL/PMWUP2jWlr2jBn2vvH5leUdhuxeNw+Ogmlw+SNmcudniO8MjG6NeSdXVtzXquDPtfePzKl+ljrtKmApbVQ5Km2i0DrRabaLQPQm2hA9KhCBUISoBCEqAQhKgEiVBQIo5HUpCuXFHRA2TGtbusx2h43K3MW6MDTTw0miPtVZ18PzXbxO96VJjcO2TOCSPVI+tqOgy63R5dNis5TpKxw7STiT1H2XFrRZzak7nnf5+CtsNxqKY0bshzm0SbpwIZtQIAHPpssdxPDGN2rXsHIFrmHSqBzUdudaG+5abhnBxmicz1mv8AR5G3W+UElw1GlX0yleO2Yst3wviWMELKZGB7LM15nEbmgdteQ1vQFPPaKUOqRrCK2jzH1jVetr3jT8iebjfEoAY4Y7JjDtRQGha1400qnXVUKO1KlxOIIbnblOYuIOoLtfqijVdbGo5hXK542au121GHxGMkN5WRNs+0CXVy0vfnZrw62OHY5opzy88yQ0fBooLO8CcMxleaL7+sTdH1rGjbzHfKN91fR4hrhbSCNtF6sLubWJy5Nc5QukTTItqhEpa95LXEGqIF7X+af+nj/wCuT+BSMKkFJ2OccR//ADl/g/qpsA45SSCLJNEUdzSfaXMgmBS2oQ5LmQS5ktqHMlzIJcyAVFmS5kEtoUeZCDrQgJUAlSJUCoQhAJUIQCQuCa+UDdUPE+JgEgC9PcSQB8ygu2zAmgQVFi9Fx8NxAJ8brvAJFruxos+SmxQcanyRk3W2p7/kstLiQ5wa4vBF05jsrswFgAjY2QP3m9Una7i7xI5uWmtBGY6Aitbt1EHXyb4hZoY9w9nYFpZmLrHNgBq+pA3A1BoCsZZ/Sbd/FvSBzmYlpmjdQjf6vpI7BLbcLthAvLY3saJ3CJyZWNjIDWDTerblIIrloR87XBheI4h8jmSw+ka5o0beUNO2aqzVlZVbZaGwqHhuaGZmhc0O9bXLYJpzS6xlBzZSdKvluuGeO6y1OJEbBbG2KNsDvaPonHLrte19M1UqSTHTEPfTiAbaSXtc4WAA1rRQsF2+gbGedE3mDwjZnsAIyHM/M9t+rlFWCMpsPB7gXH9kdPG+DMHrtLnk+qWOdQDmht5W1kJ9YjYAUSOi1MelZISYh1F7nuAvQOzaVndrqBfMtFVYO61/ZLFucPbFDdltJb4gGxr1H4VV8ObM52R7TbdRJVA/eb9V169LvWwHM0ODhjiFMAB57nlXMk/3ra6YY/ZI7pJnOlZGwgXbnno0EAAd5JPuK7ZJi2yboFjB0t72svTpmJ8lTYR3pJJiN4zhxfdZJ/nN+KuceQ1pvX1oyPHMK+NJle3WRFxfiLomBwAJLg31tdgSTp5D39FT/wDMD/st+P5qftGwhgH2Xht/uEn45is1MapaxvSWL8doX/Zb8fzQ7tC/7Lfj+ao49UyQkGqV2i/HH5Pst+P5p0vaQtA9QEuOUa0OtnyCoQVz4l3rtHQE+en5lNjUM7QOP1R8V1xcaB3CyTHKxgwM8jHPjbmDfaANu8m7lXaNE3i7O9Ss4i03QcaFmhdAbk92qxjsXQ71T8Znkc5jmwYsuZq3EYZxDm37ceXKRRppvyTY9KHFo+/3IXno4zLyxGOb+zJw+CV4+9JpmQqPXwlXPPjY2UHvAJ2HP3Jn/E4eTrU3F1XYlXH/AMSZ3nyTTxRvQ/BNw1XfSKVf/wAVH2fion8ZaN8o8XKc4vGrakjlQv7TQg16WK9TWYXoLOl9FW4jt3ggDeLg8nA/IqfJicKusXKLon2jTfEaryzinatzMTJH6MEBzm3mN+rsdu5XWN7dcOJBOKvKbGVj3a1XJptZTFT8JlldKJZy5xJOVjgLO/tAKfJivx16T2S4n+kNgkyhpfCXZQbAyyPZv+4tZP8AgvIWdo8LhooCyOctZnZGBlsVlc67dsS8HxtR4n/E/wCzh5b5W4N/lWZ5Ivx16FxTgsEvrPYHG7Gba76HTkNVk8XwBmbM99kWaq/Wcbc6+bia8g0Vobz/AP6h4t20OUH7UrnfDKrPgPGJMQ55kDRlDSMt6kurWz0Tnjb6S+PraPG46Zv0MTAxt2cx9rY2CQbdobJ1rkqbi8L4y/ODIS5zfVLiH0A4Ve9ANO2nPcLZcXgYGk5RqdfbN2NBladT3AHwVLJM1paXsc7UFrjXrNA9ZlgmzVkXRq9tbZYuVi37OcHdC6Qh7KcbDRfx5URXq8qFULCssRgJnE/SNAu6yk6nc7hM4OfWKtnLXGVrSuw2De0EOkza6erVd3tFUnaDjLMM1zna1trVnp8FqWwvkdkZVkXZ2A6leccc4TiH41rcU5sMbXNIke5uR7rBDWEuAcToK5a2r66WN72dgexwEjC0zszOvdr3tzBp8A0DxCtpRnkY3kHsvy9b/aqw8ckbK2GSL1gGFrgHHM06jSvavStwu/G430MjHuY76RxLB0IbqHDcE2QBqSSFjTW0Haln0UhP1ZGO97iz/eFjnOtXHaAYmSOXOx1ksyBurdJGnQjSxRvwKpSwsOV4pwqwe9aiUrJaTnPs2oHAWaShy0joaVxYiUekJ6AD+/gp86onz2XHq53wNfggvOFtMslfVbq6vgPP81o2cPhLswa5jvtMe5hHQg3XRU/ZtuWIOO7yT5CwPlfmreKcB3x/8nkudvbUh2M4dM0ZWvZKP+4Zncw9WyNGboNfcosM/FRktbBhjzytLw46a01z9Ttt7iu6OQnOdfVHmS4fRgdBv7gNF0YjDjKLH1nAnpWUdOoKSmlK7tHRqSOdjx7TR6MAHuDxm96FrMPghlbnmeHUNLBoHYa67UhVFF2kP0kf3JPmxVkbt1YdpT9JH9yT5sVZGd/Bc/L+zr4/RMdO9sUjmE5gxxFHmBpSwOMfxFwJM8wH+a4fJy3mIwjpmOiaadI0sB6F3qg/FRYP/DbEj2pz5hh/FYkt9N2ye3mGJwsxYc2JkcbGhkeeeu5XA/C5SDI067Fw38Cd16/xPsni4C1sUU+JsEkxPgja3lTjIW6+Fqx4b/h5HMwSYxjo5Ddxl4lIAOlvaa1GtDa1qY5/cZuWP9eYdmYmmWtvUlHvieFZYXsi6RodHBLI07OAGU0aNOJrcEeS9Lm7DYLDRukjaczR31r6p0voSoocMI2hjCQ0XTRQAs2aFVuSVLON/JZ+U6YB3+HeLcbbh/RjT23xdP2Xn5KDifZDF4YNc5rH5jQETi8iheoA0C2vEeDulfmGKxEQoDJE6Nviczoy7XxXeyFgAB9agBbnFxNDc3zUueOuoTGsZh8BL6Bjf0dznZnmnBzcttj1rvr4Lj4lwLEBnpJY2xRt3e9zWNFkAW5x01oea9c7KtAdLQrRm37yvpWAinAEdCLHdoVvx+LlNs5+TV0+fuGdnZsS0vwzmStDspcxwcA4AGi4aXRHvV/w7guIwgLpmgB+g1B1GvIr0/Fua2hYAHkFlu1WKjcxoY9riHahrgSND0W/jkvtnnaqjigAXPcABqXONAdSSdt0x/aPhDow2XE4e63Ba5zTyIIBoqu4nh/S4eWPm6NwHjVt+NLyGGF8gPo2Of0ytLvkurk93wuN9FuLJ0HQablQ8Wxb52ej9LJECRmdEQ1+UbiyOfdSjlge7LTTv0rkeqH4N/SvErHJvSTH41sOFMeCzRO9GW6blxGWO3E2SDsb5rK9gBG+Q4jFOdM+N5MVuJAdzzg/WAc067aGrpSduD6KBofqx7ix1XochLbOnMX5LN9hHH0rY2A2+y45iQKDydK0FZd7Nga8lZrSXe294/Fi8RifTQ8QdA0EGNga45PVokkOAdz3Gxrrd6+cTCA4iYySwNaPSUGhzxo6QsGgJ8VTy4WRoJI252PzTYsNI5rXNFtdZaQW0aNGjfI6J1TWl5x2FmKibGZ3Rua8PD2HXS7FEkUb2N7BPxeEimovDXOAAL9WuNDnR1VG3BTWNKs1ZIAF9TenmrqPspjujR4v/JXSbQu4JHyB8nH8SueXgzRsXDzB/BTcV4ZisM1rpCMpNW03R3o2B0PuVYOIP6oGYrhDqNPPjV/kqLE8InaKiMb/ALxLD8AVo28QcpG8TPcgqI5pYmgel0AGj2kAdRmo6ea6YuLvFF0ZIvdnrg1XIbbdV3SYtjt2Dy/ooRh8KdSwg9QSPxWeK7dOE7RQmrcW1JG51g/VczTTTYO960OH45gXANlnDTmkLXOD2g24uFkiryjms8MNhj9d/g/LIP8AWDXkVycV4XFM2jMxoAIFQt0JIIcDm0Irl11saJMS1szxhjiXZt3OrwBIb8KQslHBhAAB6XQV7RQs8K1yi97St+kjP7Dx7y38lVQ81ouP4fO0ECy2/csjiA8EZbFmuaZ4buzDLXS24V+uj/zI/wCZq9CpePYTiczPWLhYJrSiKOhsc1d8N4qXxyzYiRzmR5bAobkDfzHNZwyuM9N5YzK+3ojpmDdzR4kBQycRhbqXiuupHvC8zm7YYUezE533pXEfw6hcUvbMXceEh8SwuPvsLXyZfxnhi9F4txrDvidGyQFzgK1HUHbf4KiJc7YOP7r/AJ5a+KxuI7Y48/qwGjo1jR82lRQ8dxUjJfSzStlpvomgyBp9YZ7y+r7N7rnnLld10x1Oo3P6PJ9hw7zkA95dfwXNO9jfblhb4ytv+ENPzXnkkc7zb5Cfj8SgcMefrO+Kz8c/q863n/MsMANYtouryRyOJrajdcylk462RoP6VIb5F0Y+GrgsKzgz9xvvvX4LobwiS71HeP6K8YltXeLkiDXFpL6rU2T61/a+7v3rPOxjr1B8yrXD4KRocCbDm1qKqiCD/fVMPDeuquN49RMpv21XZHhLcRhxIXFrsxboGkUADzF3r1T+PcNxGGjzwRuxOoBjjbleB1Dc1O8lZ9hI8uGLaqpHfysWiXXHCZTdcrlZdPPeA4jEYlryMHPEWEAtnaIibv2cx9YaLqxkT42l0sT2tG7tC0eJBpbYqq7S/wDTSfu/ztUy8cktaxztsjCY5+GxEZZbXtdVi7BrUKDgnCoYH3EwNsUTua6WSsFxbCeje5taAmvBVgx0zPYle3wc4fAFcZu/brdR7LxCMPYWPZmad6cWn5EH4JOD4z0MYhh+gaH57c1hzOOhBItrRTW9LvqF5ZgO0WLA/XE+IafjVrQcK7Qzve1ji0g9xvbxpdJnlixwleoOdPIPo7fmHrRZwAf2o3EgEfsk2O9dWFwOOr1y/wAHPBr+FxBXLBwWeJ2aKcWL3bl360VxdpOP4nAxCbEv+jLg0uYXmibqxyuit7/srH/LGhGAxp9W2ZdxmN0fJt+dqPHdl/TC5C1sgAqRljN3SAjXx3+SxnDO3kOJcGQyl73XTBmLjQJNNIBOgJ8lZS8VI9vM0/tNI+aXyReFV+J4Zkc5hOrSQeY06HooDgHciPkrFsweTTgSdVKzDOK5fJr7b+P/ABT/AKFJ0B8/zSOwzx9U+WvyWhZhK0JXR+jMG4tT5l+JknNcNwR70AraYgBsbsoA9Q7V0KxgC64Z8nPLDidmQkpKtsaafFcWl5Bo8ifxVNiZ5HEEgHKTWgG4pWT2FQuh7/78lyttdZJGYk4W6yfWN3udNVZ4PCAYKaGvWkkYcvcCw/7VZ+hPKipmQ6ahZaZJnBXdAuhnBncyPJapuHCUxN6p2jOR8IbzXQzhrOiujSTOEVUDhrOgUzMGByXcSEw6qCJmHClEA6IAPL5Jr53jorApi7k30Z6D+/FM/Sig4vqFUXfZ7GxxNex5ykvLhoSKytG4Fcir2PEsd7L2nuBF+5YV2MHRVs2KuYA5spYSaOgoirHPmrPJcekvjmXb1Aqr7Rj/ANu/93+dqycGPkZ+rlruJI+Gy6MTxeZ7DG+nA1eUtvQgjl1CZeXeNmkx8erLt552vgA9ZYWaSzovZn4bDOv00Wazf0gOndpY94K5p+zuEePoajPRoa4fNhHuXPx613XTOXfUeVYVpDbcCPEUr7gLh6WPx/Aq+xfYx5eD9C5untPkYe/Qsy+V+abDgooZmxU3PY9n1gLF3mGm3RdMpNe2Mbd+nthCQt6p5CKXdwcI4Vhw8SiCISNvLIGMDxYINOqxYJHmutPpFIM/xSJoxEdNA+ifdAC/WYo8PuO9T8Y/6hn+U/8AnYueHcLweb969vi/SHP3U0p1HvUD9/76KaUajwK57bJjpMsLnfZY4+5pKw+H7VA+2xp/vzW14k0ugeANTG8DzYV44cG9poivHRejxeq4+Rux2iw3OJv+n8kLGNwslf1Quuo57r1cAdUxzRyKlCPNZaQmMp7B3pxBUbj3IJDfVAUWZycJDzCBXAKItHVSZmlHo0ENf3ScpMncmuYPBQAPcglKGpciK5pImncKCTBjkV3kKBxbyCIrZcG/kbXBO1zXucGnWgNOQH9VeklMcAs33tqemclnPMFckmNcPZHxWnlw7TuAuCfhjDs33K8k0z8nHZxz9+vzRH2mcNHxtPlXyVrLwK9tPFcOI7Pv+yHeCbhqpIu0kJ3DmfdN/NdTeKwu19ID95uvyKzs/CCN2OHvXI/AuHsOo8rF/BTjjV5WPS8H2jnoFsoeD90/HdWUXapw/WRjxFj815AyOZoFmyOY0XXDxadn1nfP5rX5T1U1L7j2KDtRA7ex7j86PwXdHxfDn/5APGx8SvHIe0TvrtafKviF3RcciPJzfA6K885/qcMK9E4s9rp48pB+ifsQfrs6LnaQ2i7QDroqOON0IDnyuIfo0CtOup16Luw7GnWrPUkk+8rhcbneTrLMJp2enaScoLvAfiVGcVI6QxgNaQ0HmTrtV6bDvXXE6twfLX5Lkxjmtmilad7jd/M3/emWHGbMct3Tmn4biJPalce7YedED4Lmd2Ykdoch8b/JagyNHentkC7SRzrIjsS7q0dwc6kLYiUdUKoqT4lA81CQeqcA5BISmZwm5+RCWggXMEvmm5AkczpogWwOSc0BNyJzQopSmG080ka0IGVqpAlyhN9H0RCuBQG+CaXVuUCYd6KSSEKI4crqBB2KR4PVQcboT3Job/YXT6NNMSioMqQtHRTGFw/qmXRoj4KKgdED/Vc03Do3btB8FZhoThh75oM3N2fYdiQVxTdnJORBWzOESDCO6/BXtOmEd2dk5t07qJ9y68DwWC63PQ6H+FbIYat0rsGx248+fvTY5JMPnjYOh5+FKSDBgb35afJdkeGDWhoJNdU8RFZxmo1l3UUcbVIcKw1mY01qLG3gnhilYO9aZOATg1AB5UiirGS5e5CKQqKqPZK9CFQoSlCFAj0hKEIpl+t5KdCEQhTCByQhRT2lDtkqEHOAkduhCjRj1PAUiFBOQkdshCqGs3UhH4JEKEFJrUqEVJEVK7ZIhVlAU1qEIqeM6JRv/fehCgedkQgV5IQqU5ieEISIQoQhaR//2Q==" alt=""  >
         <p style=" width:400px ;float:right; margin-right:30px ;margin-top:30px" >
           El blanco , unos dicen nunca falla , otros que mancha mucho , en realidad si dispones de una buena entrada de luz y optas por el blanco
          , el resultado será un espacio muy armonioso , muebles gaveteros ademas de ser practicos si ademas combina el frente con el color de las paredes 
          es mas que un acierto ,a la hora de elegir cuadros o marcos debemos de tener bastante cuidado ,tratar que todos tengan un sentido
         </p>
      </div>

      <div class=" bg-dark text-white " style="  margin-left:95px; height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
         <img style="width:290px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:30px;margin-bottom:30px "  src="https://st.hzcdn.com/simgs/2d912f3a0cc74d7a_14-5376/home-design.jpg" alt=""  >
         <p style=" margin-bottom:25px; width:400px ;float:right; margin-right:30px ;margin-top:30px" >
            Una vez fui invitada a casa de unos amigos ,ellos tenian en su salon un ventilador de techo , nada de otro siglo pasado , como seguro te estas 
            imaginando ,tenía forma de helice de avión ,como el de la imagen, pense "esta tan logrado" y la obligada era "enfría en verano?",
            en un verano de Sevilla <i class="fas fa-fire-alt"></i> recuerdo que fue conectarlo y enfriar el espacio en cuestion de minutos,asi que si estas 
            buscando una alternativa mas saludable y economica al aire acondicionado, esta es totalmente recomendada
            <a href="https://www.youtube.com/watch?v=Yt8LvHLDB20">Aqui unos cuantos</a>
         </p>
      </div>

      <div class=" bg-dark text-white " style=" margin-left:95px; height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
        <img  style="width:250px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:65px "  src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ4C5uSiGDI7owLcYIR-DTc4NzuvnPlWScHISSOhatZVEWij2Kz&usqp=CAU" alt="">
        <p style="width:450px ;float:right; margin-right:30px ;margin-top:30px" >
            En cuanto al suelo ... el suelo radiante para el invierno es sencillamente insuperable, calienta la casa al instante, mantiene el calor y evita la instalacion de radiadores y tener que dejar
            expuestas tuberias o parecidos para aquellos que les gusta llevar el detalle al maximo ,si bien la instalación puede ser el doble de costosa que un sistema de radiadores ,con el ahorro en la 
            <strong>notable</strong> en la factura es posible recuperar la inversion inicial en tercer invierno de uso , ademas de estar un paso mas cerca de conseguir una hogar eficiente.
            <a href="https://www.youtube.com/watch?v=I0oieGd_VwU">"Para muestra un boton"</a>
       </p>
      </div>

      <div class=" bg-dark text-white " style=" margin-left:95px; height:350px ; width:800px ;border-radius:10px;margin-bottom:30px" >
         <img  style="width:290px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:75px "  src="https://www.tendenciasydecoracion.com/wp-content/uploads/Un-sal%C3%B3n-con-cocina-abierta-perfecto-5.jpg" alt=""  >
         <p style=" width:400px ;float:right; margin-right:30px ;margin-top:30px" >
          Salon y cocina en un mismo espacio , este concepto que ya no es nada nuevo , me resulto especialmente atractivo la primera vez que lo ví, la idea de poder servir lo que cocinas sin 
          tener que pasar por un pasillo o abrir una puerta mientras vas cargad@ con una fuente o unos platos es realmente comodo, ademas para los fanaticos de los espacios diáfanos como yo 
          esteticamente es una idea inmejorable. Sencillamente no le veo ninguna pega a esta idea ,lo unico instala una buena campana estractora de humos ya que no es agradable que el sofa tenga
          ese aroma de aquel sofrito espectacular que preparaste.
         </p>
      </div>

      


   </div>
   <div  class="my-auto mx-auto " >
      <h3 id="cocinas" style="font-style:unset ;text-align:center; " class="my-2 text-dark"  >Cocinas</h3>
      <div style="text-align:center" >
         <a style="margin-left: 22px;" href="#baños" class="badge badge-success">Baños</a>
         <a href="#salon" class="badge badge-info">Salon</a>
         <a href="#mys" class="badge badge-warning">Inicio</a>
      </div>

      <div class=" bg-dark text-white " style=" margin-left:95px; height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
         <img  style="width:290px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:35px "  src="https://www.docrysdc.es/wp-content/uploads/2019/11/Modelo-Santos-Line-Line-E-Blanco-Blanco-Innsbruk-Blanco-Nata1-e1576801425177.jpg" alt=""  >
         <p style=" width:400px ;float:right; margin-right:30px ;margin-top:30px" >
            La siguiente opinion te puede parecer algo personal , pero el diseño italiano marca tendencia incluso en el diseño de cocinas donde los <strong>costados , frentes y puertas</strong>
            son de 1.7 cm , costados mas anchos pueden ser mas llamativos pero tener un costado con un ancho igual o parecido al ancho de las lineas de las 
            puertas es una opcion muy elegante , nada destaca por encima de nada ,todo en equilibrio y por supuesto una encimera del mismo ancho, haran de tu cocina un espacio unico en el que pasaras las 
            horas "muertas".
         </p>
      </div>

      <div class=" bg-dark text-white " style=" margin-left:95px; height:340px ; width:800px ;border-radius:10px;margin-bottom:30px" >
         <img  style="width:290px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:65px "  src="https://santossantiago.com/wp-content/uploads/2019/11/cubertero-para-cajones-de-cocina-santiago-interiores.jpg" alt=""  >
         <p style=" width:400px ;float:right; margin-right:30px ;margin-top:30px" >
            Si tu cocina trae de por si un cubertero de plastico , o directamente no encargaste este accesorio mejor incluso , un cubertero de madera de teca , resistente al agua es una opcion que te sorprendera cada 
            vez que habras el cajon de tu cocina ,hay muchas opciones para estos accesorios , dependiendo del tamaño de la gabeta asi sera tu cubertero y tendra mas o menos opciones ,
            mi consejo es situarlo en el mueble de la placa .Si alguna vez habres la gabeta y te encuentras tus cubiertos calientes ,no te asustes, esto se debe a que la placa respira por su parte de abajo , por eso 
            no te recomiendo uno de plastico que se pueda calentar.
         </p>
      </div>

      <div class=" bg-dark text-white " style=" margin-left:95px; height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
         <img  style="width:290px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:45px "  src="https://www.portalrivas.com/images/schmidt/schmidt-cocinas15.jpg" alt=""  >
         <p style=" width:400px ;float:right; margin-right:30px ;margin-top:45px" >
            Si eres un aficionado del vino ,este es tu post , recorrer tu cocina y ver al final un espacio de madera fina que mantiene tus vinos a una temperatura especifica debe ser una satisfaccion enorme,
            hoy en dia estan tan avanzados aunque no sea igual , puede reproducir de una manera bastante fiel las condiciones en las que un vino se conserva en una bodega. Un toque de distinccion que hara tu cocina unica
            <a href="https://www.youtube.com/watch?v=VFqABsU8Qz4">Echa un vistazo</a>
         </p>
      </div>

     

      <div class=" bg-dark text-white " style=" margin-left:95px; height:300px ; width:800px ;border-radius:10px;margin-bottom:30px" >
         <img  style="width:290px; border-radius:10px ; float:left  ; margin-left:30px ;margin-top:45px "  src="https://i.pinimg.com/originals/b7/95/a2/b795a2ae3dfa927810d159a556e272fb.jpg" alt=""  >
         <p style=" width:400px ;float:right; margin-right:30px ;margin-top:45px" >
            Las traseras de una cocina ... aqui llegamos al punto donde muchos discrepan , mi eleccion ... trasera de piedra siempre , no tiene nada que ver con que las traseras de madera no resisten el agua ni salpicaduras
            , esto tiene que ver mas bien con las temperaturas a las que puede llegar una cocina si se hace bastanta uso de ella, tener unas traseras de piedra que sean capaces de bajar la temperatura mientras cocinas es imprescindible.
         </p>
      </div>
 
      <div style="text-align:center ;margin-bottom:50px" >
         <a style="margin-left: 22px;" href="#baños" class="badge badge-success">Baños</a>
         <a href="#salon" class="badge badge-info">Salon</a>
         <a href="#mys" class="badge badge-warning">Inicio</a>
         <a href="#cocinas" class="badge badge-dark">Cocinas</a>

      </div>



   </div>     




   

 <!--SCRIPTS-->
 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
         integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" 
         crossorigin="anonymous">
      </script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
         integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
         crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
          integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
           crossorigin="anonymous">
      </script>
</body>
          