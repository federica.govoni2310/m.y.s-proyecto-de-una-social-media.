<?php include("connect.php");

if(isset($_GET['id'])){
    $id = $_GET['id'];

    $consulta_editpro = "SELECT * FROM profesional WHERE id=$id";
    $resultado_editpro = mysqli_query($conn , $consulta_editpro);

    if(mysqli_num_rows($resultado_editpro) == 1){
        $row= mysqli_fetch_array($resultado_editpro);
        $nombre = $row['nombre'];
        $especialidad = $row['especialidad'];
        $descripcion_pro = $row['descripcion_pro'];

        $contraseñap = $row['contraseñap'];

    };
};

if(isset($_POST['enviar_edit_pro'])){
    $nombre_updated = $_POST['nombre'];
    $especialidad_updated = $_POST['especialidad'];
    $descripcion_pro_updated = $_POST['descripcion_pro'];
    $contraseña_updated = $_POST['password_editpro'];

    if($contraseña_updated == $contraseñap){
        $consulta_updated_pro = "UPDATE profesional set nombre = '$nombre_updated' , especialidad =
         '$especialidad_updated' , descripcion_pro ='$descripcion_pro_updated',contraseñap = '$contraseña_updated'
          WHERE id=$id";
        $resultado_updated_pro = mysqli_query($conn , $consulta_updated_pro);

        if(!$resultado_updated_pro){
            die("ERROR AL EDITAR PERFIL");
        };

        $_SESSION['message'] = "Perfil actualizado";
        $_SESSION['message_type'] ="success";

        header('Location:index.php');
        
    }else{
        $_SESSION['message'] = "Contraseña incorrecta";
        $_SESSION['message_type'] = "danger";

        header('Location:index.php');
    };

};

?>

<?php include("includes/header.php") ?>
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto my-2 ">
            <div class="card card-header bg-dark text-white">Actualice su perfil</div>
            <div class="card card-body">
                 <form action="editpro.php?id=<?php echo$_GET['id']; ?>" method="POST">
                     <div class="form-group">
                         <input type="text" name="nombre" autofocus value="<?php echo$nombre ?>" class="form-control"  >
                     </div>
                     <div class="form-group">
                         <input type="text" name="especialidad" value="<?php echo$especialidad ?> " class="form-control" >
                     </div>
                     <div class="form-group">
                         <input type="text" name="descripcion_pro" class="form-control" value="<?php echo$descripcion_pro ?>" >
                     </div>
                     <div class="form-group">
                         <input type="password" class="form-control" name="password_editpro" id="password_edit_pro" placeholder="Introduzca contraseña"  >
                         <i id="boton_edit_pro" class="fas fa-eye btn btn-light my-1"></i>
                     </div>
                     <div class="contenido_edit_pro form-control bg-dark text-white">
                         Introduzca contraseña
                     </div>
                     <input type="submit" id="boton_pro_update" class="btn btn-success btn-block" name="enviar_edit_pro" value="Actualizar perfil" >
                 </form>
            </div>

        </div>
    </div>
</div>
<?php include("includes/footer.php") ?>
