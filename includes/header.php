<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>M.Y.S</title>
    
    
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
     crossorigin="anonymous">
    
     <!-- fontawsome-5 -->
     <script src="https://kit.fontawesome.com/3bbdd6b5d0.js" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="estilos.css">
</head>
<body style="background: url(https://freshome.com/wp-content/uploads/2019/05/bad-living-room-cleaning-habits.jpg)
     ;" >
<div id="contenedor"  class="container my-2">
    <nav id="nav"  class="" >
       <a id="link" class="navbar navbar-brand" href="index.php"> M.Y.S </a>
       <p style="height:70px" id="parra"  >M.Y.S es una plataforma donde podràs contactar con profesionales , tiendas y fàbricas del sector de las 
           reformas, completa tu perfil de <strong>cliente</strong> y sùbelo a nuestra Pool donde encontraràs 
           profesionales que acepten tu reto , si eres <strong>profesional</strong> sube tu perfil a nuestra Pool 
           y contacta con tus clientes .
       </p>
       <h5 id="name" >Make by your-self</h5>
       <div style="float:left" class="btn btn-group" >
         <button id="cliente_boton"  class="btn btn-success btn-sm" >Clientes</button>
         <button id="profesional_boton" class=" btn btn-success btn-sm " >Profesionales</button>
        

       </div>
       
       <ul  style=" width:650px ;float:right ;margin-right:20px " class="nav nav-tabs">

         <!--<li class="nav-item">
             <a class="nav-link text-white "  href="pools.php">Pools</a>
         </li>-->
         
         <li class="nav-item">
             <a class="nav-link text-white "  href="tendencias.php">Tendencias</a>
         </li>

         <li class="nav-item">
             <a class="nav-link text-white " style="font-size: 13px;" href="#">Para una mejor experiencia</a>
         </li>

         <li class="nav-item">
             <a class="nav-link text-white"  href="#">Quienes somos?</a>
         </li>
         
      </ul>
       
    </nav>
</div>