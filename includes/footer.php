  <!--SCRIPTS-->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
     integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" 
     crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
     integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
     crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
     integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
     crossorigin="anonymous">
    </script>
   <script>

    $(document).ready(function () {

      //variables
      var contenedor = $('.contenedor');
      var contenedor_pro =$('.contenedor_pro');
      var tabla = $('.tabla');
      var tablap = $('.tablap');
      var cliente_boton = $('#cliente_boton');
      var profesional_boton =$('#profesional_boton');
      var cliente_pool =$('#cliente_pool');
      var profesional_pool =$('#profesional_pool');
      var ojo_cliente =$('#ojo_cliente');
      var keyup = $('.keyup');
      var contraseñac = $('#contraseñac');
      var boton_check_password = $('#boton_check_password')
      var see_check_password = $('.see_check_password');
      var header_tabla = $('.header_tabla');
      var header_tablap = $('.header_tablap');
      var boton_contra_pro = $('#boton_contra_pro');
      var keyup_pro = $('.keyup_pro');
      var contraseñap = $('#contraseñap');
      var boton_contenido_cliente = $('#boton_contenido_cliente');
      var contenido_cliente = $('.contenido_cliente');
      var contraseña_cliente_final = $('#contraseña_cliente_final');
      var check_password = $('#check_password');
      var boton_edit_pro = $('#boton_edit_pro');
      var contenido_edit_pro = $('.contenido_edit_pro');
      var password_edit_pro = $('#password_edit_pro');
      var boton_pro_toggle = $('#boton_pro_toggle');
      var see_check_password_pro = $('.see_check_password_pro');
      var contraseñap_final =$('#contraseñap_final');
      var boton_pro_update = $('#boton_pro_update');
      var boton_borrar = $('#boton_borrar');
      var header_cliente = $('.header_cliente');
      var tabla_update = $('.tabla_update');
      
      // Los estilos

      
     

      $('#prescindible').css({display:'none'});
      $(contenedor).css({display:'none'});
      $(contenedor_pro).css({display:'none'});
      tabla.css({display:'none'});
      tablap.css({display:'none'});
      header_tabla.css({display:'none'});
      header_tablap.css({display:'none'});
      keyup.css({display:'none'});
      keyup_pro.css({display:'none'});
      contenido_cliente.css({display:'none'});
      see_check_password.css({display:'none'});
      contenido_edit_pro.css({display:'none'});
      see_check_password_pro.css({display:'none'});

      //Eventos click
      boton_pro_toggle.on('click',function(){
        see_check_password_pro.toggle();
      });

      boton_edit_pro.on('click',function(){
        contenido_edit_pro.toggle();

      })

      boton_contenido_cliente.on('click',function(){
        contenido_cliente.toggle();
      });

     cliente_boton.on('click',function(){
       $(contenedor).toggle();
       $(contenedor).css({background:'#34495e',color:'black'});
       tabla.toggle();
       header_tabla.toggle();
       header_tablap.hide();
       tablap.hide();
       contenedor_pro.hide();
     });

     profesional_boton.on('click',function(){
       contenedor_pro.toggle();
       tablap.toggle();
       header_tablap.toggle();
       $(contenedor_pro).css({background:'#34495e',color:'black'});
       contenedor.hide();
       tabla.hide();
       header_tabla.hide();
      });

      ojo_cliente.on('click',function(){
        keyup.toggle();
      })

      boton_contra_pro.on('click',function(){
        keyup_pro.toggle();
      });

      profesional_pool.on('click',function(){
        tablap.toggle();
        tabla.hide();
        header_tabla.hide();
        header_tablap.toggle();
      });

      boton_check_password.on('click',function(){
        see_check_password.toggle();
      });

     

      //eventos keyup

      contraseñap_final.keyup(function(){
        see_check_password_pro.html(contraseñap_final.val());
        if(contraseñap_final.val() === ''){
          see_check_password_pro.html("Esperando contraseña");
        };
      });

      contraseña_cliente_final.keyup(function(){
        contenido_cliente.html(contraseña_cliente_final.val());
        if(contraseña_cliente_final.val() === ''){
          contenido_cliente.html("Esperando contraseña");
        };
      });

      contraseñac.keyup(function(){
        keyup.html(contraseñac.val());
        if(contraseñac.val() === ''){
          keyup.html("Esperando contraseña");
        };
      });

      contraseñap.keyup(function(){
        keyup_pro.html(contraseñap.val());
        if(contraseñap.val() === ''){
          keyup_pro.html("Esperando contraseña");
        };
      });

      check_password.keyup(function(){
        see_check_password.html(check_password.val());
        if(check_password.val() === '' ){
          see_check_password.html("Esperando contraseña");
        };
      });

      password_edit_pro.keyup(function(){
        contenido_edit_pro.html(password_edit_pro.val());
        if(password_edit_pro.val() === ''){
          contenido_edit_pro.html("Esperando contraseña");
        };
      });



      

    });
      
     
   </script>
</body>
</html>