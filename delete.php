<?php include('connect.php');

if(isset($_GET['id'])){
    $id=$_GET['id'];
    $consulta_select = "SELECT * FROM cliente WHERE id=$id";
    $resultado_select = mysqli_query($conn , $consulta_select);

    if(mysqli_num_rows($resultado_select) == 1){
        $row = mysqli_fetch_array($resultado_select);
        $contraseña = $row['contraseñac'];
    };
};


   
if(isset($_POST['enviar_pass_client'])){
    $test = $_POST['test'];
    $password_check = $_POST['password_check'];

    if($test === $password_check){
        $consulta_delete = "DELETE FROM cliente WHERE id=$id";
        $resultado_delete = mysqli_query($conn , $consulta_delete);

        if(!$resultado_delete){
            die("error al borrar los datos");
        };

        $_SESSION['message'] ="Perfil borrado";
        $_SESSION['message_type'] = "success";

        header("Location:index.php");
    }else{
        $_SESSION['message'] = "Contraseña incorrecta";
        $_SESSION['message_type'] = "danger";

        header("Location:index.php");
    };
};

?>

<?php include("includes/header.php"); ?>

<div class=" contenedor_passclient container"  >
    <div class="row">
        <div class="col-md-4 mx-auto my-2 ">
            <div class="card card-header bg-dark text-white">
                Introduzca contraseña del proyecto
            </div>
            <div class="card card-body">
                <form action="delete.php?id=<?php echo$_GET['id'] ?>" method="POST" > 
                    <div class="form-group">
                        <input id="prescindible" name="test" type="password" class="form-control" value="<?php echo"$contraseña"; ?>" >
                    </div>
                    <div class="form-group">
                        <input name="password_check" type="password" class="form-control" id="check_password" placeholder="Introduzca contraseña" >
                        <i id="boton_check_password" class="fas fa-eye btn btn-light my-1"></i>
                    </div>

                    <div class="see_check_password  form-control bg-dark text-white" >
                         Esperando contraseña
                    </div>
                    <input type="submit" name="enviar_pass_client" class="btn btn-success btn-block my-2" value="Borrar Proyecto">
                </form>
            </div>
        </div>
    </div>
</div>

<?php include("includes/footer.php"); ?>
