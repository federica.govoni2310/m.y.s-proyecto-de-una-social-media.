<?php include("connect.php"); ?>

<?php include("includes/header.php") ?>

<div class="container">
          <?php if(isset($_SESSION['message'])){ ?>
              <div class="alert alert-<?= $_SESSION['message_type'] ?> alert-dismissible fade show" role="alert">
                  <?= $_SESSION['message']; ?>
                  <button type="button" class="close " data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                  </button>
             </div>
           <?php session_unset(); }; ?>
        <div style="float:left"    class="contenedor  col-md-4">
         <div class="card">
           <div  id="proyect" class=" card card-header  ">Tu proyecto</div>
            <div class="  card card-body">
               
                <form  action="procesador.php" method="POST" >
                    <div class="form-group">
                        <input type="text" name="espacio" placeholder="Espacio" class="form-control" autofocus  >
                    </div>
                    <div class="form-group">
                        <input type="text" name="medidas" placeholder="Medidas" class="form-control">
                    </div>
                    <div class="form-group">
                        <textarea name="descripcion" class="form-control" rows="2" placeholder="Describe tu proyecto"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="password"  name="contraseñac" id="contraseñac" class="form-control" placeholder="Cree contraseña" ><i id="ojo_cliente" class="fas fa-eye btn btn-light my-1"></i>
                    </div>
                    <div class="form-group">
                        <div class=" keyup bg-dark text-white form-control" style="border-radius: 10px; padding-left:5px" > Tu contraseña </div>
                    </div>
                    <input id="client_submit" type="submit" name="enviar" class="btn btn-success btn-block" value="Subir proyecto a la Pool">
                     
                </form>

            </div>
            </div>
        </div>
        
        <div  style="float:right ;" class="col-md-8">
            <div class="container">
            <table style="border-radius:10px"   class="tabla  table table-hover  bg-white text-dark table-borderless" >
                <h4 class="header_tabla " style= "  background-color:#34495e;color:white;padding-top:7px;text-align:center;border-radius:10px;height:45px;" >Pool de clientes  </h4>
                <thead>
                    <tr>
                        
                        <th>Espacio</th>
                        <th>Medidas</th>
                        <th>Descripcion</th>
                        <th>Creado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $consulta= "SELECT * FROM cliente" ;
                          $result = mysqli_query($conn , $consulta);

                          while($row = mysqli_fetch_array($result)){  ?>
                           
                          <tr>
                              <td><?php echo $row['espacio']; ?></td>
                              <td><?php echo $row['medidas']; ?></td>
                              <td style="font-size:12px" ><?php echo $row['descripcion']; ?></td>
                              <td style="font-size: 12px;" ><?php echo $row['fecha'] ?></td>
                              <td>
                                  <a id="editClient" class="  btn btn-light btn-sm" href="edit.php?id=<?php echo $row['id'] ?>"><i   class="fas fa-marker"></i></a>
                                  <a class="btn btn-danger btn-sm "  href="delete.php?id=<?php echo $row['id'] ?>"><i class="fas fa-trash-alt"></i></a>

                              </td>

                          </tr>

                    <?php  }; ?>
                   
                  

                     
                     
                    
                </tbody>
            </table>
            </div>
        </div>
            <div    class="container">
                
                <?php if(isset($_SESSION['message'])){ ?>
                      <div class="alert alert-<?= $_SESSION['message_type'] ?> alert-dismissible fade show" role="alert">
                           <?= $_SESSION['message']; ?>
                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                           </button>
                      </div>
                <?php session_unset(); }; ?>

                
                   <div style="float:left ; " class=" contenedor_pro col-md-4">
                        <div class="card">
                          <div id="profile" class="card card-header">
                                En que te diferencias?
                          </div>

                          <div class="card card-body">
                              <form action="procesador.php" method="POST" >
                                  <div class="form-group">
                                      <input type="text" name="nombre" class="form-control" placeholder="Nombre"  autofocus>
                                  </div>
                                  <div class="form-group">
                                      <input type="text" name="especialidad" placeholder="Especialidad" class="form-control" >
                                  </div>
                                  <div class="form-group">
                                     <input type="text" name="descripcionpro" placeholder="Describe tus habilidades" class="form-control" >
                                  </div>
                                  <div class="form-group">
                                    <input type="password"  name="contraseñap" id="contraseñap" class="form-control" placeholder="Cree contraseña" ><i id="boton_contra_pro" class="fas fa-eye btn btn-light my-1"></i>
                                  </div>
                                  <div class="form-group">
                                     <div class=" keyup_pro bg-dark text-white form-control" style="border-radius: 10px; padding-left:5px" >
                                         Tu contraseña
                                     </div>
                                 </div>
                                 <input type="submit" class="btn btn-success btn-block" value="Sube tu perfil a la Pool" name="enviarpro" >
                               </form>
                           </div>
                        </div>
                    </div>
                
                  
                  




                  
                    <div style="float:right ;" class="col-md-8 ">
                       <table style="border-radius: 10px;"  class="tablap  table table-hover  bg-white text-dark table-borderless"  >
                         <h4 class="header_tablap  " style=" background-color:#34495e;color:white ;padding-top:7px;text-align:center;border-radius:10px;height:45px "
                          
                         >  Pool de Profesionales  </h4>
                           
                          <thead>
                              <tr>
                                  <th>Nombre</th>
                                  <th>Especialidad</th>
                                  <th>Habilidades</th>
                                  <th>Creado</th>
                                  <th>Acciones</tr>
                              </tr>
                          </thead>
                          <tbody>
                              <?php $selectp = "SELECT * FROM profesional";
                                    $resultadop = mysqli_query($conn , $selectp);

                                    while($row = mysqli_fetch_array($resultadop)){  ?>
                                       <tr>
                                           <td><?php echo$row['nombre'] ?></td>
                                           <td><?php echo$row['especialidad'] ?></td>
                                           <td style="font-size: 12px;" ><?php echo$row['descripcion_pro'] ?></td>
                                           <td  style="font-size: 12px;" ><?php echo$row['fecha_pro'] ?></td>
                                           <td ><a class="btn btn-light btn-sm" href="editpro.php?id=<?php echo $row['id'] ?>"><i class="fas fa-marker"></i></a>
                                                <a id="delete_pro" class="btn btn-danger btn-sm "  href="deletepro.php?id=<?php echo $row['id'] ?>"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                             <?php }; ?>
                          </tbody>
                       </table>
                    </div>
                </div>
            </div>
        
    </div>
</div>

<?php include("includes/footer.php") ?>