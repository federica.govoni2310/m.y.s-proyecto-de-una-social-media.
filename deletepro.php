<?php include("connect.php");

if(isset($_GET['id'])){
    $id = $_GET['id'];

    $consulta_pro = "SELECT * FROM profesional WHERE id=$id";
    $resultado_pro = mysqli_query($conn , $consulta_pro);

    if(mysqli_num_rows($resultado_pro) == 1 ){
        $row= mysqli_fetch_array($resultado_pro);
        $contraseñap = $row['contraseñap'];

    };
};


if(isset($_POST['enviar_pro_test'])){
    $contraseñap_final= $_POST['contraseñap_final'];
 
   if($contraseñap_final === $contraseñap){
       $consulta_delete_pro = "DELETE FROM profesional WHERE id = $id";
       $resultado_delete_pro = mysqli_query($conn , $consulta_delete_pro);
 
       if(!$resultado_delete_pro){
          die("ERROR AL BORRAR PERFIL");
         }
 
       $_SESSION['message'] = "Perfil borrado";
       $_SESSION['message_type'] = "info";
 
        header("Location:index.php");
     }else{
       $_SESSION['message'] = "Contraseña incorrecta";
       $_SESSION['message_type'] ="danger";
 
        header("Location:index.php");
    };
};

?>

<?php include("includes/header.php") ?>
<div class="container">
    <div class="row">
        <div class="col-md-4 mx-auto my-2 ">
            <div class="card card-header  bg-dark text-white">Introduzca contraseña del perfil</div>
            <div class="card card-body">
                <form action="deletepro.php?id=<?php echo$_GET['id'] ?>" method="POST" >
                  
                    <div class="form-group">
                        <input type="password" name="contraseñap_final" id="contraseñap_final" placeholder="Introduzca contraseña" class="form-control" >
                        <i id="boton_pro_toggle" class="fas fa-eye btn btn-light my-1"></i>
                    </div>
                    <div class="see_check_password_pro form-control bg-dark text-white" >
                         Esperando contraseña
                    </div>
                    <input type="submit" name="enviar_pro_test" id="boton_borrar" class="btn btn-success btn-block" value="Borrar perfil"  >
                </form>
            </div>
        </div>
    </div>
</div>
<?php include("includes/footer.php") ?>
